# GreenPass Decoder

European GreenPass QRCode is a coded inside complicated format, which can be read by classic scanning apps.

This project eanble you to read your QRCode by deploying a website at [https://greenpass.codable.tv](https://greenpass.codable.tv). Go to this website and scan your QRCode to know what is coded inside.

## QRCode format

As explained by EHC specification, data is coded inside QRCode follow this algorithms:

![workflow](https://github.com/ehn-dcc-development/hcert-spec/blob/main/overview.png)


